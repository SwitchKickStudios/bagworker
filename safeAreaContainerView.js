import React, { Component } from 'react';
import { SafeAreaView, StyleSheet, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import TabNavigator from './Components/tabNavigator';
import * as gb from './globalConstants';

class SafeAreaContainerView extends Component {
    isItChocolateMode() {
        const { chocolateMode } = this.props;
        return chocolateMode;
    }

    render() {
        return (
            <SafeAreaView style={[styles.safeArea, this.isItChocolateMode() && styles.chocolateSafeArea]}>
                <React.Fragment>
                    <StatusBar
                        barStyle="light-content"
                    />
                    <TabNavigator />
                </React.Fragment>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: gb.darkerRedColor
    },
    chocolateSafeArea: {
        flex: 1,
        backgroundColor: gb.chocolateNavigationColor
    }
});


const mapStateToProps = state => ({ chocolateMode: state.timer.chocolateMode });

export default connect(mapStateToProps)(SafeAreaContainerView);
