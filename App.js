import React from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import AppReducer from './Components/AppReducer';
import SafeAreaContainerView from './safeAreaContainerView';

class App extends React.Component {
    store = createStore(AppReducer);

    render() {
        return (
            <Provider store={this.store}>
                <SafeAreaContainerView />
            </Provider>
        );
    }
}

AppRegistry.registerComponent('App', () => App);

export default App;
