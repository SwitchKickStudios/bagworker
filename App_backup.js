import { createBottomTabNavigator } from 'react-navigation';
import Timer from './Components/timer';
import Setup from './Components/setup';

const TabNavigator = createBottomTabNavigator({
    Timer,
    Setup
});

export default TabNavigator;
