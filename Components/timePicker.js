import React, { Component } from 'react';
import { StyleSheet, Platform } from 'react-native';
import {
    Icon,
    Picker,
} from 'native-base';
import { connect } from 'react-redux';
import { Font } from 'expo';
import {
    roundMinutesChanged,
    roundSecondsChanged,
    restMinutesChanged,
    restSecondsChanged,
} from './timerActions';
import * as gb from '../globalConstants';
import globalStyles from '../styling/globalStyles';

const normalFont = require('../assets/fonts/Azonix.otf');
const chocolateFont = require('../assets/fonts/Choko.ttf');

class TimePicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.selectedValue()
        };
    }

    async componentDidMount() {
        await Font.loadAsync({
            NormalFont: normalFont,
            Choko: chocolateFont
        });
        this.setState({ fontLoaded: true });
    }

    onValueChange = (value) => {
        if (this.props.type === 'roundMinutes') {
            this.props.roundMinutesChanged({ value });
        } else if (this.props.type === 'roundSeconds') {
            this.props.roundSecondsChanged({ value });
        } else if (this.props.type === 'restMinutes') {
            this.props.restMinutesChanged({ value });
        } else if (this.props.type === 'restSeconds') {
            this.props.restSecondsChanged({ value });
        }
        this.setState(() => (
            Object.assign(
                { value }
            )
        ));
    }

    createPickerOptions = () => {
        const options = [];
        if (this.props.type === 'roundMinutes' || this.props.type === 'restMinutes') {
            for (let i = 0; i <= 60; i += 1) {
                const labelString = String(i);
                options.push(
                    <Picker.Item label={labelString} value={i} key={i} />
                );
            }
        } else if (this.props.type === 'roundSeconds' || this.props.type === 'restSeconds') {
            for (let i = 0; i <= 60; i += 5) {
                let labelString = String(i);
                if (labelString.length === 1) {
                    labelString = `0${labelString}`;
                }
                options.push(
                    <Picker.Item label={labelString} value={i} key={i} />
                );
            }
        }

        return options;
    }

    selectedValue() {
        let value = 0;
        switch (this.props.type) {
        case 'roundMinutes':
            value = this.props.roundMinutes;
            break;
        case 'roundSeconds':
            value = this.props.roundSeconds;
            break;
        case 'restMinutes':
            value = this.props.restMinutes;
            break;
        case 'restSeconds':
            value = this.props.restSeconds;
            break;
        default:
            break;
        }
        return value;
    }

    isItChocolateMode() {
        const { chocolateMode } = this.props;
        return chocolateMode;
    }

    renderIcon() {
        if (this.isItChocolateMode()) {
            return (
                <Icon
                    name="ios-arrow-down-outline"
                    style={{
                        color: gb.chocolateFontColor,
                        textShadowColor: 'rgba(0, 0, 0, 1)',
                        textShadowOffset: { width: -1, height: 4 },
                        textShadowRadius: 0
                    }}
                />);
        }
        return (
            <Icon
                name="ios-arrow-down-outline"
                style={{
                    color: 'white',
                    textShadowColor: 'rgba(0, 0, 0, 1)',
                    textShadowOffset: { width: -1, height: 4 },
                    textShadowRadius: 0
                }}
            />);
    }

    render() {
        return (
            this.state.fontLoaded ? (
                <Picker
                    mode="dropdown"
                    textStyle={[styles.pickedText, globalStyles.smallTextShadow, this.isItChocolateMode() && chocolateStyles.chocolateFont]}
                    style={[styles.picker, this.isItChocolateMode() && chocolateStyles.picker]}
                    iosIcon={this.renderIcon()}
                    selectedValue={this.state.value}
                    onValueChange={value => (this.onValueChange(value))}
                >
                    {this.createPickerOptions()}
                </Picker>
            ) : null
        );
    }
}

const styles = StyleSheet.create({
    pickedText: {
        fontSize: 44,
        paddingRight: 0,
        color: 'white',
        fontFamily: 'NormalFont'
    },
    picker: {
        width: undefined,
        height: 100,
        ...Platform.select({
            android: {
                color: 'white'
            }
        })
    },
    arrowIcon: {
        color: 'black'
    }
});

const chocolateStyles = StyleSheet.create({
    chocolateFont: {
        color: gb.chocolateFontColor,
        fontFamily: 'Choko',
        fontSize: 56
    },
    picker: {
        ...Platform.select({
            android: {
                color: gb.chocolateFontColor
            }
        })
    },
});

const mapStateToProps = state => ({
    roundMinutes: state.timer.roundMinutes,
    roundSeconds: state.timer.roundSeconds,
    restMinutes: state.timer.restMinutes,
    restSeconds: state.timer.restSeconds,
    chocolateMode: state.timer.chocolateMode
});

export default connect(mapStateToProps, {
    roundMinutesChanged,
    roundSecondsChanged,
    restMinutesChanged,
    restSecondsChanged,
})(TimePicker);
