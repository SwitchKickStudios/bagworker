import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    Switch,
    Slider,
    Platform
} from 'react-native';
import {
    Container,
    Content,
    Form,
    Item,
} from 'native-base';
import { connect } from 'react-redux';
import { Font } from 'expo';
import { debounce } from 'lodash';
import { chocolateModeChanged, setAppVolume } from './timerActions';
import SetupLabel from './setupLabel';
import RoundPicker from './roundPicker';
import TimePicker from './timePicker';
import * as gb from '../globalConstants';
import globalStyles from '../styling/globalStyles';

const normalFont = require('../assets/fonts/Azonix.otf');
const chocolateFont = require('../assets/fonts/Choko.ttf');

class Setup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fontLoaded: false,
            chocolateMode: this.isItChocolateMode(),
            appVolume: this.currentAppVolume()
        };
        this.debounceVolumeChange = debounce(this.onChangeVolume, 12);
        this.debounceVolumeChangeComplete = debounce(this.onVolumeSlideComplete, 10);
    }

    async componentDidMount() {
        await Font.loadAsync({
            NormalFont: normalFont,
            Choko: chocolateFont
        });
        this.setState({ fontLoaded: true });
    }

    onValueChange = (value) => {
        this.props.chocolateModeChanged({ value });
        this.setState(() => (
            Object.assign(
                { chocolateMode: value }
            )
        ));
    }

    onVolumeSlideComplete = () => {
        const { appVolume } = this.state;
        this.props.setAppVolume({ value: appVolume });
    }

    onChangeVolume = (value) => {
        this.setState(() => ({ appVolume: value }));
    }

    getCurrentFontColor() {
        if (this.isItChocolateMode()) {
            return gb.chocolateFontColor;
        }
        return 'white';
    }

    isItChocolateMode() {
        const { chocolateMode } = this.props;
        return chocolateMode;
    }

    currentAppVolume() {
        const { appVolume } = this.props;
        return appVolume;
    }

    canChangeTimerTotals() {
        const { canChangeTimerTotals } = this.props;
        return canChangeTimerTotals;
    }

    render() {
        return (
            this.state.fontLoaded ? (
                <Container style={[styles.container, this.isItChocolateMode() && chocolateStyles.container]}>
                    <Content>
                        <Form>
                            {/* Number of Rounds */}
                            <Item fixedLabel style={[styles.item, this.isItChocolateMode() && chocolateStyles.item]}>
                                <SetupLabel type="fixed" itIsChocolateMode={this.isItChocolateMode()} text="Number of Rounds:" />
                                <RoundPicker />
                            </Item>

                            {/* Round Time */}
                            <SetupLabel type="top" itIsChocolateMode={this.isItChocolateMode()} text="Round Length:" />
                            <Item style={[styles.item, styles.timerInput, this.isItChocolateMode() && chocolateStyles.item]}>
                                <TimePicker type="roundMinutes" />
                                <Text style={[styles.colon, globalStyles.smallTextShadow, this.isItChocolateMode() && chocolateStyles.colon]}>:</Text>
                                <TimePicker type="roundSeconds" />
                            </Item>

                            {/* Rest Time */}
                            <SetupLabel type="top" itIsChocolateMode={this.isItChocolateMode()} text="Time Between Rounds:" />
                            <Item style={[styles.item, styles.timerInput, this.isItChocolateMode() && chocolateStyles.item]}>
                                <TimePicker type="restMinutes" />
                                <Text style={[styles.colon, globalStyles.smallTextShadow, this.isItChocolateMode() && chocolateStyles.colon]}>:</Text>
                                <TimePicker type="restSeconds" />
                            </Item>

                            {/* Volume Control */}
                            <SetupLabel type="top" itIsChocolateMode={this.isItChocolateMode()} text={`Volume: ${this.state.appVolume}`} />
                            <Item style={[styles.item, styles.timerInput, this.isItChocolateMode() && chocolateStyles.item]}>
                                <Slider
                                    style={styles.volumeSlider}
                                    minimumValue={0}
                                    maximumValue={100}
                                    step={1}
                                    value={this.state.appVolume}
                                    onValueChange={value => this.debounceVolumeChange(value)}
                                    onSlidingComplete={this.debounceVolumeChangeComplete()}
                                    minimumTrackTintColor={this.getCurrentFontColor()}
                                    maximumTrackTintColor="#282828"
                                />
                            </Item>

                            {/* Chocolate Mode */}
                            <Item fixedLabel style={[styles.item, this.isItChocolateMode() && chocolateStyles.item]}>
                                <SetupLabel type="fixed" itIsChocolateMode={this.isItChocolateMode()} text="Chocolate Mode:" />
                                <Switch
                                    value={this.state.chocolateMode}
                                    onValueChange={value => this.onValueChange(value)}
                                    style={styles.switch}
                                    disabled={!this.canChangeTimerTotals()}
                                />
                            </Item>
                        </Form>
                    </Content>
                </Container>
            ) : null
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 0,
        backgroundColor: gb.primaryRedColor,
        ...Platform.select({
            android: { marginTop: 26 }
        })
    },
    item: {
        height: 80,
        borderColor: gb.darkerRedColor
    },
    timerInput: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    colon: {
        fontSize: 40,
        paddingBottom: 8,
        color: 'white',
        fontFamily: 'NormalFont'
    },
    switch: {
        marginRight: 15
    },
    volumeSlider: {
        width: '80%'
    }
});

const chocolateStyles = StyleSheet.create({
    container: {
        backgroundColor: gb.chocolateMainBackgroundColor,
    },
    item: {
        borderColor: gb.chocolateNavigationColor
    },
    colon: {
        color: gb.chocolateFontColor,
        fontFamily: 'Choko'
    }
});

const mapStateToProps = state => ({
    chocolateMode: state.timer.chocolateMode,
    canChangeTimerTotals: state.timer.canChangeTimerTotals,
    appVolume: state.timer.appVolume
});

export default connect(mapStateToProps, {
    chocolateModeChanged,
    setAppVolume
})(Setup);
