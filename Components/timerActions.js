export const roundsChanged = rounds => ({
    type: 'ROUNDS_CHANGED',
    payload: rounds
});

export const roundMinutesChanged = roundMinutes => ({
    type: 'ROUND_MINUTES_CHANGED',
    payload: roundMinutes
});

export const roundSecondsChanged = roundSeconds => ({
    type: 'ROUND_SECONDS_CHANGED',
    payload: roundSeconds
});

export const restMinutesChanged = restMinutes => ({
    type: 'REST_MINUTES_CHANGED',
    payload: restMinutes
});

export const restSecondsChanged = restSeconds => ({
    type: 'REST_SECONDS_CHANGED',
    payload: restSeconds
});

export const chocolateModeChanged = chocolateMode => ({
    type: 'CHOCOLATE_MODE_CHANGED',
    payload: chocolateMode
});

export const canChangeTimerTotalsChanged = canChangeTimerTotals => ({
    type: 'CAN_CHANGE_TIMER_TOTALS_CHANGED',
    payload: canChangeTimerTotals
});

export const setNextTimerTotals = () => ({
    type: 'SET_NEXT_TIMER_TOTALS'
});

export const setAppVolume = volume => ({
    type: 'SET_APP_VOLUME',
    payload: volume
});
