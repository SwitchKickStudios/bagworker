import { combineReducers } from 'redux';
import TimerReducer from './timerReducer';

const AppReducer = combineReducers({
    timer: TimerReducer,
});

export default AppReducer;
