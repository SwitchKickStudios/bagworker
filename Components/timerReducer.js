const initialState = {
    rounds: 1,
    nextNumberOfRounds: 1,
    roundMinutes: 1, // should be set to 1 for production
    roundSeconds: 0,
    nextRoundMinutes: 1, // should be set to 1 for production
    nextRoundSeconds: 0,
    restMinutes: 0,
    restSeconds: 0,
    nextRestMinutes: 0,
    nextRestSeconds: 0,
    chocolateMode: false,
    canChangeTimerTotals: true,
    appVolume: 100
};

const TimerReducer = (state = initialState, action) => {
    switch (action.type) {
    case 'ROUNDS_CHANGED':
        if (state.canChangeTimerTotals) {
            return {
                ...state,
                rounds: action.payload.value,
                nextNumberOfRounds: action.payload.value
            };
        }
        return { ...state, nextNumberOfRounds: action.payload.value };
    case 'ROUND_MINUTES_CHANGED':
        if (state.canChangeTimerTotals) {
            return {
                ...state,
                roundMinutes: action.payload.value,
                nextRoundMinutes: action.payload.value
            };
        }
        return { ...state, nextRoundMinutes: action.payload.value };
    case 'ROUND_SECONDS_CHANGED':
        if (state.canChangeTimerTotals) {
            return {
                ...state,
                roundSeconds: action.payload.value,
                nextRoundSeconds: action.payload.value
            };
        }
        return { ...state, nextRoundSeconds: action.payload.value };
    case 'REST_MINUTES_CHANGED':
        if (state.canChangeTimerTotals) {
            return {
                ...state,
                restMinutes: action.payload.value,
                nextRestMinutes: action.payload.value
            };
        }
        return { ...state, nextRestMinutes: action.payload.value };
    case 'REST_SECONDS_CHANGED':
        if (state.canChangeTimerTotals) {
            return {
                ...state,
                restSeconds: action.payload.value,
                nextRestSeconds: action.payload.value
            };
        }
        return { ...state, nextRestSeconds: action.payload.value };
    case 'CHOCOLATE_MODE_CHANGED':
        return { ...state, chocolateMode: action.payload.value };
    case 'CAN_CHANGE_TIMER_TOTALS_CHANGED':
        return { ...state, canChangeTimerTotals: action.payload.value };
    case 'SET_NEXT_TIMER_TOTALS':
        return {
            ...state,
            rounds: state.nextNumberOfRounds,
            roundMinutes: state.nextRoundMinutes,
            roundSeconds: state.nextRoundSeconds,
            restMinutes: state.nextRestMinutes,
            restSeconds: state.nextRestSeconds
        };
    case 'SET_APP_VOLUME':
        return { ...state, appVolume: action.payload.value };
    default:
        return state;
    }
};

export default TimerReducer;
