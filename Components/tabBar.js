import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { createBottomTabNavigator } from 'react-navigation';

class TimerTab extends Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>Timer</Text>
            </View>
        );
    }
}

class SetupTab extends Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>Setup</Text>
            </View>
        );
    }
}

const TabNavigator = createBottomTabNavigator({
    TimerTab,
    SetupTab
});

export default TabNavigator;
