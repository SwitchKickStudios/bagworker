import { Audio } from 'expo';

const goBeepSoundPath = require('../assets/audio/GoBeep.wav');
const restSoundPath = require('../assets/audio/RestSound.wav');
const tickBeepSoundPath = require('../assets/audio/TickBeep.wav');

class AudioService {
    static async playGoBeep(volume) {
        const sound = new Audio.Sound();
        try {
            await sound.loadAsync(goBeepSoundPath);
            await sound.setVolumeAsync(volume);
            await sound.playAsync();
        } catch (error) {
            console.log(error);
        }
    }

    static async playRestSound(volume) {
        const sound = new Audio.Sound();
        try {
            await sound.loadAsync(restSoundPath);
            await sound.setVolumeAsync(volume);
            await sound.playAsync();
        } catch (error) {
            console.log(error);
        }
    }

    static async playTickBeep(volume) {
        const sound = new Audio.Sound();
        try {
            await sound.loadAsync(tickBeepSoundPath);
            await sound.setVolumeAsync(volume);
            await sound.playAsync();
        } catch (error) {
            console.log(error);
        }
    }
}

export default AudioService;
