import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Alert
} from 'react-native';
import { Font, Audio } from 'expo';
import { connect } from 'react-redux';
import { Button } from 'native-base';
import TimeFormatter from 'minutes-seconds-milliseconds';
import AudioService from './audioService';
import { canChangeTimerTotalsChanged, setNextTimerTotals } from './timerActions';
import * as gb from '../globalConstants';
import globalStyles from '../styling/globalStyles';

const timerFont = require('../assets/fonts/Azonix.otf');
const chocolateFont = require('../assets/fonts/Choko.ttf');

class Timer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fontLoaded: false,
            mainTimer: null,
            displayTime: null,
            mainTimerStart: null,
            restTimer: null,
            displayRestTime: null,
            restTimerStart: null,
            isRunning: false,
            restIsRunning: false,
            timeHasStarted: false,
            restTimeHasStarted: false,
            currentRound: 1,
            timeHasEnded: false,
            canPlayCountDown: [false, false, false],
        };
        this.handleStartStop = this.handleStartStop.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }

    async componentDidMount() {
        // Load Fonts
        await Font.loadAsync({
            timerFont,
            Choko: chocolateFont
        });
        this.setState({ fontLoaded: true });

        // Set Audio Parameters
        Audio.setAudioModeAsync({
            allowsRecordingIOS: false,
            interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DUCK_OTHERS,
            interruptionModeAndroid: Audio.INTERRUPTION_MODE_IOS_DUCK_OTHERS,
            playsInSilentModeIOS: true,
            shouldDuckAndroid: false,
        });
    }

    setNextRound() {
        const currentRound = this.state.currentRound;
        const nextRound = currentRound + 1;
        this.setState({
            currentRound: nextRound
        });
    }

    calculateSeconds = (seconds) => {
        const milliseconds = seconds * 1000;
        return milliseconds;
    }

    calculateMinutes = (minutes) => {
        const milliseconds = minutes * 1000 * 60;
        return milliseconds;
    }

    currentAppVolume() {
        const { appVolume } = this.props;
        const adjustedAppVolume = appVolume / 100;
        return adjustedAppVolume;
    }

    selectedRoundTime() {
        const { roundMinutes, roundSeconds } = this.props;
        const roundTime = this.calculateMinutes(roundMinutes) + this.calculateSeconds(roundSeconds);
        return roundTime;
    }

    selectedRestTime() {
        const { restMinutes, restSeconds } = this.props;
        const restTime = this.calculateMinutes(restMinutes) + this.calculateSeconds(restSeconds);
        return restTime;
    }

    selectedNumberOfRounds() {
        const { rounds } = this.props;
        return rounds;
    }

    isItChocolateMode() {
        const { chocolateMode } = this.props;
        return chocolateMode;
    }

    processNextRestPeriod() {
        clearInterval(this.interval);
        const { restTimer } = this.state;
        this.setState({
            timeHasStarted: false
        });
        this.startRestTimer(restTimer);
        AudioService.playRestSound(this.currentAppVolume());
    }

    processNextRound() {
        clearInterval(this.interval);
        this.setState({
            restTimer: 0,
            restTimerStart: new Date(),
            restIsRunning: false,
            restTimeHasStarted: false,
            mainTimer: 0,
            mainTimerStart: new Date(),
            timeHasStarted: true
        });
        this.setNextRound();
        this.startMainTimer(this.state.mainTimer);
    }

    startRestTimer(restTimer) {
        this.setState({
            restTimerStart: new Date(),
            restIsRunning: true,
            restTimeHasStarted: true,
            canPlayCountDown: [true, true, true]
        });
        let restTimerUpdated = restTimer;
        this.interval = setInterval(() => {
            let startingMilliseconds = this.selectedRestTime();
            let currentRoundTimerValue = new Date() - this.state.restTimerStart + restTimerUpdated;
            let newRestTimerValue = currentRoundTimerValue;
            let newRestTimerDisplayValue = startingMilliseconds - currentRoundTimerValue;

            this.determinePlayingCoundDownTick(newRestTimerDisplayValue);

            if (newRestTimerDisplayValue <= 0) {
                this.processNextRound();
                restTimerUpdated = 0;
                startingMilliseconds = this.selectedRestTime();
                currentRoundTimerValue = new Date() - this.state.restTimerStart + restTimerUpdated;
                newRestTimerValue = currentRoundTimerValue;
                newRestTimerDisplayValue = startingMilliseconds;
            }

            this.setState({
                restTimer: newRestTimerValue,
                displayRestTime: newRestTimerDisplayValue
            });
        }, 30);
    }

    processTimerEnd() {
        clearInterval(this.interval);
        this.setState({
            mainTimerStart: null,
            mainTimer: 0,
            timeHasStarted: false,
            restTimerStart: null,
            restTimer: 0,
            restTimeHasStarted: false,
            restIsRunning: false,
            isRunning: false,
            timeHasEnded: true
        });
        AudioService.playGoBeep(this.currentAppVolume());
    }

    determinePlayingCoundDownTick(timerValue) {
        if (timerValue <= this.calculateSeconds(3) && this.state.canPlayCountDown[2]) {
            AudioService.playTickBeep(this.currentAppVolume());
            this.setState({ canPlayCountDown: [true, true, false] });
        }
        if (timerValue <= this.calculateSeconds(2) && this.state.canPlayCountDown[1]) {
            AudioService.playTickBeep(this.currentAppVolume());
            this.setState({ canPlayCountDown: [true, false, false] });
        }
        if (timerValue <= this.calculateSeconds(1) && this.state.canPlayCountDown[0]) {
            AudioService.playTickBeep(this.currentAppVolume());
            this.setState({ canPlayCountDown: [false, false, false] });
        }
    }

    startMainTimer(mainTimer) {
        AudioService.playGoBeep(this.currentAppVolume());
        this.setState({ canPlayCountDown: [true, true, true] });
        const mainTimerUpdated = mainTimer;
        this.interval = setInterval(() => {
            const startingMilliseconds = this.selectedRoundTime();
            const currentTimerValue = new Date() - this.state.mainTimerStart + mainTimerUpdated;
            const newMainTimerValue = currentTimerValue;
            let newTimerDisplayValue = startingMilliseconds - currentTimerValue;

            this.determinePlayingCoundDownTick(newTimerDisplayValue);

            if (newTimerDisplayValue <= 0 && this.showRoundsLeft() === 1) {
                this.processTimerEnd();
                newTimerDisplayValue = startingMilliseconds;
            } else if (newTimerDisplayValue <= 0) {
                if (this.selectedRestTime() > 0) {
                    this.processNextRestPeriod();
                } else {
                    this.processNextRound();
                }
                newTimerDisplayValue = startingMilliseconds;
            }
            this.setState({
                mainTimer: newMainTimerValue,
                displayTime: newTimerDisplayValue
            });
        }, 30);
    }

    handleStartStop() {
        const {
            isRunning,
            mainTimer,
            restIsRunning,
            restTimer,
            timeHasEnded
        } = this.state;

        if (!timeHasEnded) { // If after end of last round, start/stop should not do anything
            if (isRunning) { // If we are running, Pause
                clearInterval(this.interval);
                this.setState({
                    isRunning: false
                });
                return;
            }

            if (restIsRunning) { // If the timer isn't running and it is during a rest period, resume rest timer
                this.setState({
                    restTimerStart: new Date(),
                    isRunning: true,
                });
                this.startRestTimer(restTimer);
            } else if (this.selectedRoundTime() === 0) {
                Alert.alert('Invalid Round Length', 'The Round Length can\'t be 0:00.');
            } else { // If the timer isn't running and it is during a round, resume main timer
                this.setState({
                    mainTimerStart: new Date(),
                    isRunning: true,
                    timeHasStarted: true
                });
                this.props.canChangeTimerTotalsChanged({ value: false });
                this.startMainTimer(mainTimer);
            }
        }
    }

    handleReset() {
        const { isRunning, mainTimerStart, timeHasEnded } = this.state;
        if ((mainTimerStart && !isRunning) || timeHasEnded) {
            this.setState({
                mainTimerStart: null,
                mainTimer: 0,
                timeHasStarted: false,
                restTimerStart: null,
                restTimer: 0,
                restTimeHasStarted: false,
                restIsRunning: false,
                currentRound: 1,
                timeHasEnded: false,
            });
        }
        this.props.canChangeTimerTotalsChanged({ value: true });
        this.props.setNextTimerTotals();
    }

    determineTimeToShow() {
        const roundTime = this.selectedRoundTime();
        const { timeHasStarted } = this.state;
        let time = TimeFormatter(this.state.displayTime);
        if (!timeHasStarted) {
            time = TimeFormatter(roundTime);
        }
        return time;
    }

    determineMainTextToShow() {
        const { restIsRunning, timeHasEnded } = this.state;
        if (restIsRunning) {
            return 'REST';
        }
        if (timeHasEnded) {
            return 'FINISHED';
        }
        return '';
    }

    determineRestTimeToShow() {
        const restTime = this.selectedRestTime();
        const restTimeHasStarted = this.state.restTimeHasStarted;
        let time = TimeFormatter(this.state.displayRestTime);
        if (!restTimeHasStarted) {
            time = TimeFormatter(restTime);
        }
        return time;
    }

    showRoundsLeft() {
        const rounds = this.selectedNumberOfRounds();
        const currentRound = this.state.currentRound - 1;
        return rounds - currentRound;
    }

    renderMainTimerText() {
        const { restIsRunning, timeHasEnded } = this.state;
        if (restIsRunning || timeHasEnded) {
            return (
                <View style={[
                    styles.timerContainer,
                    this.isItChocolateMode() && chocolateStyles.timerContainer]}
                >
                    <Text style={[
                        styles.timerLabelText,
                        styles.oldSchoolFont,
                        globalStyles.bigTextShadow,
                        this.isItChocolateMode() && chocolateStyles.chocolateFont,
                        this.isItChocolateMode() && chocolateStyles.timerLabelText,
                        this.determineMainTextToShow() === 'REST' && styles.altTimerLabelText,
                        this.determineMainTextToShow() === 'REST' && this.isItChocolateMode() && chocolateStyles.altTimerLabelText]}
                    >
                        {this.determineMainTextToShow()}
                    </Text>
                </View>
            );
        }

        return (
            <View style={[
                styles.timerContainer,
                this.isItChocolateMode() && chocolateStyles.timerContainer]}
            >
                <Text style={[
                    styles.timerText,
                    styles.oldSchoolFont,
                    globalStyles.bigTextShadow,
                    this.isItChocolateMode() && chocolateStyles.chocolateFont,
                    this.isItChocolateMode() && chocolateStyles.timerText]}
                >
                    {this.determineTimeToShow().substr(0, 5)}
                </Text>
                <Text style={[
                    styles.millisecondText,
                    styles.oldSchoolFont,
                    globalStyles.smallTextShadow,
                    this.isItChocolateMode() && chocolateStyles.chocolateFont,
                    this.isItChocolateMode() && chocolateStyles.millisecondText]}
                >
                    {this.determineTimeToShow().substr(5, 3)}
                </Text>
            </View>
        );
    }

    render() {
        const { isRunning } = this.state;

        return (
            this.state.fontLoaded ? (
                <View style={[
                    styles.container,
                    this.isItChocolateMode() && chocolateStyles.container]}
                >
                    <View style={styles.timerSectionContainer}>
                        {this.renderMainTimerText()}

                        <Text style={[
                            styles.roundLabelText,
                            styles.oldSchoolFont,
                            globalStyles.smallTextShadow,
                            this.isItChocolateMode() && chocolateStyles.chocolateFont]}
                        >
                            Round:
                        </Text>
                        <View style={styles.roundContainer}>
                            <Text style={[
                                styles.roundText,
                                styles.oldSchoolFont,
                                globalStyles.smallTextShadow,
                                this.isItChocolateMode() && chocolateStyles.chocolateFont]}
                            >
                                {this.state.currentRound}
                            </Text>
                            <Text style={[
                                styles.roundTotalText,
                                styles.oldSchoolFont,
                                globalStyles.smallTextShadow,
                                this.isItChocolateMode() && chocolateStyles.chocolateFont]}
                            >
                                /{this.selectedNumberOfRounds()}
                            </Text>
                        </View>

                        <View style={[
                            styles.restContainer,
                            this.isItChocolateMode() && chocolateStyles.restContainer]}
                        >
                            <Text style={[
                                styles.restText,
                                styles.oldSchoolFont,
                                globalStyles.smallTextShadow,
                                this.isItChocolateMode() && chocolateStyles.chocolateFont,
                                this.isItChocolateMode() && chocolateStyles.restText]}
                            >
                                {this.determineRestTimeToShow().substr(0, 5)}
                            </Text>
                            <Text style={[
                                styles.restMillisecondText,
                                styles.oldSchoolFont,
                                globalStyles.smallTextShadow,
                                this.isItChocolateMode() && chocolateStyles.chocolateFont,
                                this.isItChocolateMode() && chocolateStyles.restMillisecondText]}
                            >
                                {this.determineRestTimeToShow().substr(5, 3)}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.buttonRow}>
                        <Button
                            success
                            style={[
                                styles.button,
                                styles.rightPadding,
                                styles.startButton,
                                this.isItChocolateMode() && chocolateStyles.chocolateStartButton,
                                isRunning && styles.stopButton,
                                this.isItChocolateMode() && chocolateStyles.chocolateButtonBorder]}
                            onPress={this.handleStartStop}
                        >
                            <Text
                                style={[
                                    styles.oldSchoolFont,
                                    styles.buttonFont,
                                    this.isItChocolateMode() && chocolateStyles.chocolateFontDark]}
                            >
                                {[isRunning ? 'Pause' : 'Start']}
                            </Text>
                        </Button>
                        <Button
                            style={[
                                styles.button,
                                styles.resetButton,
                                this.isItChocolateMode() && chocolateStyles.chocolateButtonBorder]}
                            onPress={this.handleReset}
                        >
                            <Text
                                style={[
                                    styles.oldSchoolFont,
                                    styles.buttonFont,
                                    this.isItChocolateMode() && chocolateStyles.chocolateFontDark]}
                            >
                                Reset
                            </Text>
                        </Button>
                    </View>
                </View>
            ) : null
        );
    }
}

const styles = StyleSheet.create({
    oldSchoolFont: {
        fontFamily: 'timerFont',
        color: '#FFF',
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: gb.primaryRedColor,
        paddingBottom: 24
    },
    timerSectionContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center'
    },
    timerContainer: {
        width: 320,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        marginBottom: 20
    },
    timerText: {
        fontSize: 79,
        position: 'absolute',
        left: 0,
    },
    timerLabelText: {
        fontSize: 60,
        textAlign: 'center'
    },
    altTimerLabelText: {
        fontSize: 88
    },
    millisecondText: {
        fontSize: 32,
        paddingBottom: 8,
        position: 'absolute',
        right: 0
    },
    roundLabelText: {
        fontSize: 35
    },
    roundContainer: {
        flexDirection: 'row',
    },
    roundText: {
        fontSize: 60,
        textAlign: 'center',
    },
    roundTotalText: {
        fontSize: 35,
        paddingTop: 26,
    },
    restContainer: {
        width: 225,
        flex: 1,
        marginTop: 18
    },
    restText: {
        fontSize: 54,
        position: 'absolute',
        left: 0,
    },
    restMillisecondText: {
        fontSize: 26,
        paddingTop: 22,
        position: 'absolute',
        right: 0
    },
    // BUTTONS
    buttonRow: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 26
    },
    button: {
        width: 140,
        height: 46,
        justifyContent: 'center',
        marginTop: 60,
        borderWidth: 3,
        borderColor: 'white',
    },
    startButton: {
        backgroundColor: gb.startButtonColor
    },
    stopButton: {
        backgroundColor: gb.stopButtonColor
    },
    resetButton: {
        backgroundColor: gb.resetButtonColor
    },
    buttonFont: {
        fontSize: 20,
        paddingTop: 4
    },
    rightPadding: {
        marginRight: 50
    },
});

const chocolateStyles = StyleSheet.create({
    chocolateFont: {
        fontFamily: 'Choko',
        color: gb.chocolateFontColor
    },
    chocolateFontDark: {
        fontFamily: 'Choko',
        color: gb.chocolateNavigationColor
    },
    chocolateStartButton: {
        backgroundColor: '#599044'
    },
    chocolateButtonBorder: {
        borderColor: gb.chocolateNavigationColor
    },
    container: {
        backgroundColor: gb.chocolateMainBackgroundColor,
    },
    timerContainer: {
        width: 318,
    },
    restContainer: {
        width: 180
    },
    timerText: {
        fontSize: 114
    },
    timerLabelText: {
        fontSize: 76
    },
    altTimerLabelText: {
        fontSize: 110
    },
    millisecondText: {
        fontSize: 40,
        paddingBottom: 15,
    },
    restText: {
        fontSize: 60
    },
    restMillisecondText: {
        paddingTop: 26,
    }
});

const mapStateToProps = state => ({
    rounds: state.timer.rounds,
    roundMinutes: state.timer.roundMinutes,
    roundSeconds: state.timer.roundSeconds,
    restMinutes: state.timer.restMinutes,
    restSeconds: state.timer.restSeconds,
    chocolateMode: state.timer.chocolateMode,
    appVolume: state.timer.appVolume
});
export default connect(mapStateToProps, { canChangeTimerTotalsChanged, setNextTimerTotals })(Timer);
