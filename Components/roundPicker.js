import React, { Component } from 'react';
import { StyleSheet, Platform } from 'react-native';
import {
    Icon,
    Picker,
} from 'native-base';
import { connect } from 'react-redux';
import { Font } from 'expo';
import { roundsChanged } from './timerActions';
import * as gb from '../globalConstants';
import globalStyles from '../styling/globalStyles';

const normalFont = require('../assets/fonts/Azonix.otf');
const chocolateFont = require('../assets/fonts/Choko.ttf');

class RoundPicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rounds: this.selectedNumberOfRounds(),
            fontLoaded: false
        };
    }

    async componentDidMount() {
        await Font.loadAsync({
            NormalFont: normalFont,
            Choko: chocolateFont
        });
        this.setState({ fontLoaded: true });
    }

    onValueChange = (value) => {
        this.props.roundsChanged({ value });
        this.setState(() => (
            Object.assign(
                { rounds: value }
            )
        ));
    }

    createPickerOptions = () => {
        const options = [];
        for (let i = 1; i <= 99; i += 1) {
            options.push(
                <Picker.Item label={`${i}`} value={i} key={i} />
            );
        }
        return options;
    }

    selectedNumberOfRounds() {
        const { rounds } = this.props;
        return rounds;
    }

    isItChocolateMode() {
        const { chocolateMode } = this.props;
        return chocolateMode;
    }

    renderIcon() {
        if (this.isItChocolateMode()) {
            return (
                <Icon
                    name="ios-arrow-down-outline"
                    style={{
                        color: gb.chocolateFontColor,
                        textShadowColor: 'rgba(0, 0, 0, 1)',
                        textShadowOffset: { width: -1, height: 4 },
                        textShadowRadius: 0
                    }}
                />);
        }
        return (
            <Icon
                name="ios-arrow-down-outline"
                style={{
                    color: 'white',
                    textShadowColor: 'rgba(0, 0, 0, 1)',
                    textShadowOffset: { width: -1, height: 4 },
                    textShadowRadius: 0
                }}
            />);
    }

    render() {
        return (
            this.state.fontLoaded ? (
                <Picker
                    mode="dropdown"
                    textStyle={[styles.pickedText, globalStyles.smallTextShadow, this.isItChocolateMode() && chocolateStyles.chocolateFont]}
                    style={[styles.picker, this.isItChocolateMode() && chocolateStyles.picker]}
                    iosIcon={this.renderIcon()}
                    selectedValue={this.state.rounds}
                    onValueChange={value => this.onValueChange(value)}
                >
                    {this.createPickerOptions()}
                </Picker>
            ) : null
        );
    }
}

const styles = StyleSheet.create({
    pickedText: {
        fontSize: 28,
        paddingRight: 0,
        color: 'white',
        fontFamily: 'NormalFont'
    },
    picker: {
        width: undefined,
        ...Platform.select({
            android: {
                color: 'white',
            }
        })
    }
});

const chocolateStyles = StyleSheet.create({
    chocolateFont: {
        color: gb.chocolateFontColor,
        fontSize: 38,
        fontFamily: 'Choko'
    },
    picker: {
        ...Platform.select({
            android: {
                color: gb.chocolateFontColor
            }
        })
    },
});

const mapStateToProps = state => ({
    rounds: state.timer.rounds,
    chocolateMode: state.timer.chocolateMode
});

export default connect(mapStateToProps, {
    roundsChanged,
})(RoundPicker);
