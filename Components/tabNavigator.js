import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import { connect } from 'react-redux';
import { Icon } from 'native-base';
import Timer from './timer';
import Setup from './setup';
import * as gb from '../globalConstants';

class TabNavigator extends React.Component {
    isItChocolateMode() {
        const { chocolateMode } = this.props;
        return chocolateMode;
    }

    render() {
        const Nav = createBottomTabNavigator({
            Timer: { screen: Timer },
            Setup: { screen: Setup }
        }, {
            navigationOptions: ({ navigation }) => ({
                tabBarIcon: ({ tintColor }) => {
                    const { routeName } = navigation.state;
                    let iconName;
                    let iconType;
                    if (routeName === 'Timer') {
                        iconName = 'timer';
                        iconType = 'MaterialCommunityIcons';
                    } else if (routeName === 'Setup') {
                        iconName = 'sliders';
                        iconType = 'FontAwesome';
                    }

                    return (
                        <Icon
                            name={iconName}
                            type={iconType}
                            style={{
                                fontSize: 32,
                                color: tintColor,
                            }}
                        />
                    );
                }
            }),
            initialRouteName: 'Setup',
            tabBarPosition: 'bottom',
            tabBarOptions: {
                showLabel: false,
                activeTintColor: 'white',
                inactiveTintColor: gb.primaryRedColor,
                activeBackgroundColor: gb.darkerRedColor,
                inactiveBackgroundColor: gb.inactiveRedColor,
                style: {
                    backgroundColor: gb.darkerRedColor,
                },
            },
        });

        const ChocolateNav = createBottomTabNavigator({
            Timer: { screen: Timer },
            Setup: { screen: Setup }
        }, {
            navigationOptions: ({ navigation }) => ({
                tabBarIcon: ({ tintColor }) => {
                    const { routeName } = navigation.state;
                    let iconName;
                    let iconType;
                    if (routeName === 'Timer') {
                        iconName = 'timer';
                        iconType = 'MaterialCommunityIcons';
                    } else if (routeName === 'Setup') {
                        iconName = 'sliders';
                        iconType = 'FontAwesome';
                    }

                    return (
                        <Icon
                            name={iconName}
                            type={iconType}
                            style={{
                                fontSize: 32,
                                color: tintColor
                            }}
                        />
                    );
                }
            }),
            initialRouteName: 'Setup',
            tabBarPosition: 'bottom',
            tabBarOptions: {
                showLabel: false,
                activeTintColor: 'white',
                inactiveTintColor: gb.chocolateMainBackgroundColor,
                activeBackgroundColor: gb.chocolateNavigationColor,
                inactiveBackgroundColor: gb.chocolateInactiveColor,
                style: {
                    backgroundColor: gb.chocolateNavigationColor,
                },
            }
        });

        if (this.isItChocolateMode()) {
            return <ChocolateNav />;
        }
        return <Nav />;
    }
}

const mapStateToProps = state => ({ chocolateMode: state.timer.chocolateMode });

export default connect(mapStateToProps)(TabNavigator);
