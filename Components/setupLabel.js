import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Label } from 'native-base';
import * as gb from '../globalConstants';
import globalStyles from '../styling/globalStyles';

class SetupLabel extends Component {
    render() {
        return (
            <Label style={[
                this.props.type === 'fixed' && styles.fixedLabel,
                this.props.type === 'top' && styles.topLabel,
                styles.label,
                globalStyles.tinyTextShadow,
                this.props.itIsChocolateMode && chocolateStyles.chocolateFont]}
            >
                {this.props.text}
            </Label>
        );
    }
}

const styles = StyleSheet.create({
    fixedlabel: {
        paddingLeft: 0,
    },
    topLabel: {
        paddingLeft: 15,
        paddingTop: 15,
    },
    label: {
        color: 'white',
        fontFamily: 'NormalFont'
    },
});

const chocolateStyles = StyleSheet.create({
    chocolateFont: {
        fontFamily: 'Choko',
        fontSize: 20,
        color: gb.chocolateFontColor
    },
});

export default SetupLabel;
