import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    bigTextShadow: {
        textShadowColor: 'rgba(0, 0, 0, 1)',
        textShadowOffset: { width: -2, height: 8 },
        textShadowRadius: 0
    },
    smallTextShadow: {
        textShadowColor: 'rgba(0, 0, 0, 1)',
        textShadowOffset: { width: -1, height: 4 },
        textShadowRadius: 0
    },
    tinyTextShadow: {
        textShadowColor: 'rgba(0, 0, 0, 1)',
        textShadowOffset: { width: -0.25, height: 1 },
        textShadowRadius: 0
    }
});
